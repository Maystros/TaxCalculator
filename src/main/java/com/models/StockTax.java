package com.models;

import com.stock.StockPurchase;

public class StockTax {

    String stockSymbol;
    int numberOfShares;
    String dateOfPurchase;
    double purchaseValue;
    String dateOfSale;
    double saleValue;
    double gains;
    double taxes;


    public StockTax(StockSale sale) {
        this.stockSymbol = sale.getStockSymbol();
        this.dateOfPurchase = sale.getDateOfPurchase();
        this.numberOfShares = sale.getNumberOfSharesInPurchase();
        StockPurchase stockPurchase = new StockPurchase(stockSymbol, dateOfPurchase, numberOfShares);
        this.purchaseValue = stockPurchase.getValueAtPurchase();
        this.dateOfSale = sale.getDateOfSale();
        com.stock.StockSale stockSale = new com.stock.StockSale(stockPurchase, dateOfSale);
        this.saleValue = stockSale.getSaleValue();
        this.gains = stockSale.getGains();
        this.taxes = stockSale.getFlatTax();
    }
}
