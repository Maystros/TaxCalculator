package com.models;

import com.revenue.TaxCalculator;
import com.tools.PropertiesHandler;

public class RevenueTax {

    double taxableIncome;
    double defaultDeduction;
    double taxes;
    double taxPercentage;

    public RevenueTax(double taxableIncome) {
        this.taxableIncome = taxableIncome;
        this.defaultDeduction = Double.parseDouble(PropertiesHandler.getProperty("defaultDeduction"));
        this.taxes = new TaxCalculator(taxableIncome).calculateTaxOnRevenue();
        this.taxPercentage = taxes/taxableIncome;
    }
}
