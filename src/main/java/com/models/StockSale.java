package com.models;

public class StockSale {

    String stockSymbol;
    String dateOfPurchase;
    int numberOfSharesInPurchase;
    String dateOfSale;

    public StockSale(String stockSymbol, String dateOfPurchase, int numberOfSharesInPurchase, String dateOfSale) {
        this.stockSymbol = stockSymbol;
        this.dateOfPurchase = dateOfPurchase;
        this.numberOfSharesInPurchase = numberOfSharesInPurchase;
        this.dateOfSale = dateOfSale;
    }

    public String getStockSymbol() {
        return stockSymbol;
    }

    public String getDateOfPurchase() {
        return dateOfPurchase;
    }

    public int getNumberOfSharesInPurchase() {
        return numberOfSharesInPurchase;
    }

    public String getDateOfSale() {
        return dateOfSale;
    }
}
