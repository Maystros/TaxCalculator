package com.revenue;

import com.tools.PropertiesHandler;

import java.util.List;

/**
 * Class to calculate taxes given the taxable income
 * @author sehouate
 */
public class TaxCalculator {

	private final List<TaxSlice> taxSlices;
	private final double taxableIncome;

    /**
     * Create a TaxCalculator object given a taxable income
     * @param income taxable income
     */
	public TaxCalculator(double income) {
        double defaultTaxDeduction = Double.parseDouble(PropertiesHandler.getProperty("defaultDeduction"));
        this.taxSlices = TaxSlice.fetchTaxSlices();
		this.taxableIncome = income * (1 - defaultTaxDeduction);
	}

    /**
     * Calculates the tax to pay given a income
     * @return The tax to pay given an income
     */
	public double calculateTaxOnRevenue() {
        double taxToPay = 0;
        boolean calculationInitiated = false;
        for (int i = taxSlices.size() - 1; i >= 0; i--) {
            TaxSlice currentSlice = taxSlices.get(i);
            if (isIncomeInSlice(currentSlice, taxableIncome) && !calculationInitiated) {
                taxToPay = taxToPay + calculateTaxForSlice(currentSlice, taxableIncome);
                calculationInitiated = true;
            } else if (calculationInitiated) {
                taxToPay = taxToPay
                        + (currentSlice.getMax() - currentSlice.getMin()) * currentSlice.getPercentage();
            }
        }

        return Math.ceil(taxToPay);
    }

    /**
     * Calculate taxes for a given slice and a given income
     * @param taxSlice The slice to calculate the tax
     * @param income the taxable income
     * @return The taxes for the current slice
     */
    private double calculateTaxForSlice(TaxSlice taxSlice, double income) {
        if (isIncomeInSlice(taxSlice, income)){
            return (income - taxSlice.getMin()) * taxSlice.getPercentage();
        }

        throw new IllegalArgumentException(
                "The income should be in the range of the slice; ["
                        + taxSlice.getMin() + "," + taxSlice.getMax()
                        + "]");

    }

    /**
     * Check if the income is in the taxSlice
     * @param taxSlice The current slice
     * @param income the taxable income
     * @return True if income is in slice, false otherwise
     */
	private boolean isIncomeInSlice(TaxSlice taxSlice, double income) {
		return (income > taxSlice.getMin() && income <= taxSlice.getMax());
	}

}
