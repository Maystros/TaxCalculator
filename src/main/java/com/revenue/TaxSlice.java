package com.revenue;

import java.util.ArrayList;
import java.util.List;

import com.tools.PropertiesHandler;

/**
 * Class to manage tax slices
 * @author sehouate
 */
class TaxSlice {

    private final double max;
    private final double min;
    private final double percentage;

    /**
     * Constructor of the class TaxSlice
     * @param max the max taxable income in the slice
     * @param min the min taxable income in the slice
     * @param percentage the percentage of the tax
     */
    private TaxSlice(double max, double min, double percentage) {
        this.max = max;
        this.min = min;
        this.percentage = percentage;
    }

    /**
     * Returns the max of the slice
     * @return the max taxable income in the slice
     */
    double getMax() {
        return max;
    }

    /**
     * returns the min of the slice
     * @return the min taxable income in the slice
     */
    double getMin() {
        return min;
    }

    /**
     * returns the percentage of taxes in the slice
     * @return the percentage of the tax
     */
    double getPercentage() {
        return percentage;
    }

    /**
     * Returns a list of Tax slices ordered from smallest to the biggest
     * @return Ordered list of TaxSlices
     */
    static List<TaxSlice> fetchTaxSlices() {

        List<TaxSlice> taxSlices = new ArrayList<>();

        //Define first slice
        double firstSliceMax = Double.parseDouble(PropertiesHandler.getProperty("firstSliceMax"));
        double firstSliceMin = Double.parseDouble(PropertiesHandler.getProperty("firstSliceMin"));
        double firstSlicePercentage = Double.parseDouble(PropertiesHandler.getProperty("firstSlicePercentage"));
        TaxSlice slice1 = new TaxSlice(firstSliceMax, firstSliceMin, firstSlicePercentage);
        taxSlices.add(slice1);

        //Define second slice
        double secondSliceMax = Double.parseDouble(PropertiesHandler.getProperty("secondSliceMax"));
        double secondSliceMin = Double.parseDouble(PropertiesHandler.getProperty("secondSliceMin"));
        double secondSlicePercentage = Double.parseDouble(PropertiesHandler.getProperty("secondSlicePercentage"));
        TaxSlice slice2 = new TaxSlice(secondSliceMax, secondSliceMin, secondSlicePercentage);
        taxSlices.add(slice2);

        //Define third slice
        double thirdSliceMax = Double.parseDouble(PropertiesHandler.getProperty("thirdSliceMax"));
        double thirdSliceMin = Double.parseDouble(PropertiesHandler.getProperty("thirdSliceMin"));
        double thirdSlicePercentage = Double.parseDouble(PropertiesHandler.getProperty("thirdSlicePercentage"));
        TaxSlice slice3 = new TaxSlice(thirdSliceMax, thirdSliceMin, thirdSlicePercentage);
        taxSlices.add(slice3);

        //Define fourth slice
        double fourthSliceMax = Double.parseDouble(PropertiesHandler.getProperty("fourthSliceMax"));
        double fourthSliceMin = Double.parseDouble(PropertiesHandler.getProperty("fourthSliceMin"));
        double fourthSlicePercentage = Double.parseDouble(PropertiesHandler.getProperty("fourthSlicePercentage"));
        TaxSlice slice4 = new TaxSlice(fourthSliceMax, fourthSliceMin, fourthSlicePercentage);
        taxSlices.add(slice4);

        //Define fifth slice
        double fifthSliceMax = Double.parseDouble(PropertiesHandler.getProperty("fifthSliceMax"));
        double fifthSliceMin = Double.parseDouble(PropertiesHandler.getProperty("fifthSliceMin"));
        double fifthSlicePercentage = Double.parseDouble(PropertiesHandler.getProperty("fifthSlicePercentage"));
        TaxSlice slice5 = new TaxSlice(fifthSliceMax, fifthSliceMin, fifthSlicePercentage);
        taxSlices.add(slice5);

        return taxSlices;
    }

}
