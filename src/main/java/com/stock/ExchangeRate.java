package com.stock;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tools.HttpHelper;
import com.tools.PropertiesHandler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class ExchangeRate {

    private String defaultBaseCurrency = PropertiesHandler.getProperty("defaultBaseCurrency");
    private String defaultTargetCurrency = PropertiesHandler.getProperty("defaultTargetCurrency");
    private String currentDate = DateTimeFormatter.ofPattern("YYYY-MM-dd").format((LocalDate.now().minusDays(1)));

    /**
     * Advanced constructor of the class.
     *
     * @param baseCurrency   the base currency of the exchange
     * @param targetCurrency the target currency of the exchange
     * @param date           the date of the exchange in the format YYYY-MM-dd
     */
    public ExchangeRate(String baseCurrency, String targetCurrency, String date) {
        this.defaultBaseCurrency = baseCurrency;
        this.defaultTargetCurrency = targetCurrency;
        this.currentDate = date;
    }

    /**
     * Constructor based on the default properties and a given date.
     *
     * @param date the date of the exchange in the format YYYY-MM-DD
     */
    public ExchangeRate(String date) {
        this.currentDate = date;
    }

    /**
     * Default constructor, based on default properties and the current date.
     */
    public ExchangeRate() {
    }

    /**
     * Fetches exchange rate based on the European Central Bank API.
     *
     * @return the exchange rate as a double.
     */
    private double fetchExchangeRate() {

        if (Objects.equals(defaultBaseCurrency, defaultTargetCurrency)) {
            return 1;
        }
        else {
            String ratesHistory;
            String filePath = "";

            try {
                filePath = getExchangeRatesHistory().getCanonicalPath();
                ratesHistory = new String(Files.readAllBytes(Paths.get(filePath)));
            } catch (IOException e) {
                throw new IllegalStateException("Unable to parse file " + filePath);
            }
            String str = currentDate + " GMT";
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd zzz");
            try {
                long epoch = df.parse(str).getTime();
                JsonParser parser = new JsonParser();
                for (JsonElement pointInTime : parser.parse(ratesHistory).getAsJsonObject().get("HistoricalPoints").getAsJsonArray()) {
                    JsonObject obj = pointInTime.getAsJsonObject();
                    if(obj.get("PointInTime").getAsLong() == epoch) {
                        return obj.get("InterbankRate").getAsDouble();
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return 0;

//        String url = "http://api.exchangeratesapi.io/v1/" + currentDate + "?access_key=95ec91bc743570e7d1fe5d32c84bde02&symbols=" + defaultBaseCurrency;
//            String response = HttpHelper.get(url);
//            JsonParser parser = new JsonParser();
//            double exchangeRate = 1 / parser.parse(response).getAsJsonObject().getAsJsonObject("rates").get(defaultBaseCurrency).getAsDouble();
//            return exchangeRate;
    }

    void downloadExchangeRatesHistory() {
        String url = "https://api.ofx.com/PublicSite.ApiService//SpotRateHistory/10year/" + defaultBaseCurrency + "/" + defaultTargetCurrency + "?DecimalPlaces=6&ReportingInterval=daily&format=json";
        String response = HttpHelper.get(url);
        String filePath = Objects.requireNonNull(this.getClass().getClassLoader().getResource("exchangeRates/")).getPath()
                + defaultBaseCurrency + "-" + defaultTargetCurrency +  ".json";
        try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
            byte[] strToBytes = response.getBytes();
            outputStream.write(strToBytes);
        } catch (IOException e) {
            throw new IllegalStateException("There was an error writing the file " + filePath);
        }
        assert (new File(filePath)).exists();
    }

    /**
     * Get the exchange rate of the last 10 years.
     */
    File getExchangeRatesHistory() {
        URL url = this.getClass().getClassLoader().getResource("exchangeRates/");
        String filePath = Objects.requireNonNull(url).getPath()
                + defaultBaseCurrency + "-" + defaultTargetCurrency +  ".json";
        File file = new File(filePath);
        LocalDate today = LocalDate.now();
        LocalDate lastModification = Instant.ofEpochMilli(file.lastModified()).atZone(ZoneId.systemDefault()).toLocalDate();

        if (today.isAfter(lastModification)) {
            downloadExchangeRatesHistory();
        }


        assert (new File(filePath)).exists();
        return new File(filePath);
    }


    /**
     * Converts a given amount to the target currency.
     * @param price the amount in base currency to convert to target currency.
     * @return The converted amount in the target currency.
     */
    public double convertToTargetCurrency(double price) {
        return price*fetchExchangeRate();
    }
}
