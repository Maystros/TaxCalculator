package com.stock;

import com.tools.PropertiesHandler;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author sehouate
 */
public class StockSale {

    private final StockPurchase stockPurchase;
    private final String dateOfSale;
    private final ExchangeRate exchangeRate;
    private final StockHistory stockHistory;
    private final double unitarySaleValue;

    /**
     * Constructor for the Stock sale based on a purchase and a selling date.
     *
     * @param stockPurchase The stock purchase.
     * @param dateOfSale    the date of the sale.
     */
    public StockSale(StockPurchase stockPurchase, String dateOfSale) {
        this.stockPurchase = stockPurchase;
        this.stockHistory = new StockHistory(this.stockPurchase.getStockSymbol());
        this.dateOfSale = dateOfSale;
        this.exchangeRate = new ExchangeRate(this.dateOfSale);
        this.unitarySaleValue = exchangeRate.convertToTargetCurrency(
                stockHistory.fetchStockPriceAtGivenDate(stockHistory.getValidStockMarketDay(dateOfSale)));
    }

    /**
     * Constructor for the stock sale based on the purchase alone.
     * The date of the sale is then the current date.
     *
     * @param stockPurchase The stock purchase.
     */
    public StockSale(StockPurchase stockPurchase) {
        this(stockPurchase,
                new StockHistory(stockPurchase.getStockSymbol()).
                        getValidStockMarketDay(DateTimeFormatter.ofPattern("yyyy-MM-dd").format((LocalDate.now()))));
    }

    /**
     * Total gains of a purchase at the current date in the target currency.
     *
     * @return the total gains of the purchase at the current date.
     */
    private double calculateGainsOfPurchase() {
        return (exchangeRate.convertToTargetCurrency(stockHistory.fetchStockPriceAtGivenDate(dateOfSale))
                * stockPurchase.getNumberOfShares())
                - stockPurchase.getValueAtPurchase();
    }

    /**
     * Calculate taxes on the gains of a purchase in the target currency.
     *
     * @return total taxes to pay on a purchase.
     */
    private double calculateFlatTaxesOnPurchase() {
        return calculateGainsOfPurchase() * Double.parseDouble(PropertiesHandler.getProperty("defaultStockTax"));
    }

    /**
     * Calculate taxes on the gains of a purchase in the target currency.
     *
     * @return total taxes to pay on a purchase.
     */
    private double calculateGridTaxesOnPurchase() {

        return ((calculateGainsOfPurchase()/2) * 0.41);
    }

    /**
     * @return
     */
    public double getGains() {
        return calculateGainsOfPurchase();
    }

    /**
     * @return
     */
    public double getFlatTax() {
        return calculateFlatTaxesOnPurchase();
    }

    /**
     * @return
     */
    public double getGridTax() {
        return calculateGridTaxesOnPurchase();
    }

    public double getSaleValue() {
        return unitarySaleValue * stockPurchase.getNumberOfShares();
    }
}