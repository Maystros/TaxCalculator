package com.stock;

import com.tools.DateHelper;

public class Rsu extends StockPurchase {

    private final String grantDate;
    private final String vestDate;

    /**
     * Default constructor.
     *
     * @param stockSymbol    Symbol of the stock to handle
     * @param dateOfPurchase The date of purchase
     * @param numberOfShares
     * @param grantDate
     */
    public Rsu(String stockSymbol, String dateOfPurchase, double numberOfShares, String grantDate) {
        super(stockSymbol, dateOfPurchase, numberOfShares);
        this.grantDate = grantDate;
        this.vestDate = dateOfPurchase;
    }

    public boolean isQualified() {
        return DateHelper.getLocalDateFromString(grantDate).isAfter(DateHelper.getLocalDateFromString("2019-10-15"));
    }

    public double calculateTaxAtAcquisition() {
        return 0;
    }

    public double calculateTaxAtSale(String saleDate) {
        return 0;
    }

    public double calculateTax(String saleDate) {
        if (isQualified()) {
            return calculateTaxAtAcquisition() + calculateTaxAtSale(saleDate);
        }
        else {
            return calculateTaxAtSale(saleDate);
        }
    }

    public boolean isReductionEligible() {
        return DateHelper.getLocalDateFromString(vestDate).isBefore(DateHelper.getLocalDateFromString("2018-01-01"));
    }


}