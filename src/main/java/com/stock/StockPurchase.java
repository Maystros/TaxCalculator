package com.stock;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author sehouate
 */
public class StockPurchase {

    private final String stockSymbol;
    private final String dateOfPurchase;
    private final double numberOfSharesInPurchase;
    private final StockHistory stockHistory;
    private final ExchangeRate exchangeRate;

    /**
     * Default constructor.
     *
     * @param stockSymbol    Symbol of the stock to handle
     * @param dateOfPurchase The date of purchase
     */
    public StockPurchase(String stockSymbol, String dateOfPurchase, double numberOfShares) {
        this.stockSymbol = stockSymbol;
        exchangeRate = new ExchangeRate(dateOfPurchase);
        this.stockHistory = new StockHistory(stockSymbol);
        this.dateOfPurchase = dateOfPurchase;
        this.numberOfSharesInPurchase = numberOfShares;
    }

    /**
     * Returns the date of the purchase.
     * @return date of purchase.
     */
    public String getDateOfPurchase() {
        return this.dateOfPurchase;
    }

    /**
     * Returns the total value of the purchase at the time of purchase in the target currency.
     *
     * @return the total value of the purchase at the time of purchase.
     */
    public double getValueAtPurchase() {
        return exchangeRate.convertToTargetCurrency(calculateTotalStockValueAtPurchase());
    }

    /**
     * Returns the current market value of the purchase in the target currency.
     *
     * @return the current market value of the purchase.
     */
    public double getCurrentValue() {
        return (new ExchangeRate()).convertToTargetCurrency(calculateTotalCurrentStockValue());
    }

    /**
     * Returns the market value of the purchase at a specific date in the target currency.
     *
     * @param date date in the format yyyy-MM-dd.
     * @return the market value of the purchase.
     */
    public double getValueAtDate(String date) {
        return (new ExchangeRate(date)).convertToTargetCurrency(calculateTotalStockValueAtDate(date));
    }

    /**
     * Returns the stock symbol of the stock purchase.
     *
     * @return the stock symbol as a string
     */
    public String getStockSymbol() {
        return stockSymbol;
    }

    /**
     * Returns the number of shares in the purchase.
     *
     * @return the number os shares in the purchase.
     */
    public double getNumberOfShares() {
        return numberOfSharesInPurchase;
    }

    /**
     * Calculate total value at purchase in the original currency of the stock.
     *
     * @return total value of the purchase at date of purchase.
     */
    private double calculateTotalStockValueAtPurchase() {
        return stockHistory.fetchStockPriceAtGivenDate(dateOfPurchase) * numberOfSharesInPurchase;
    }

    /**
     * Total value of stock at the current date in the original currency of the stock.
     *
     * @return total value of the purchase at the current date.
     */
    private double calculateTotalCurrentStockValue() {
        String currentDate = stockHistory.getValidStockMarketDay(DateTimeFormatter.ofPattern("yyyy-MM-dd").format((LocalDate.now())));
        return stockHistory.fetchStockPriceAtGivenDate(currentDate) * numberOfSharesInPurchase;
    }

    /**
     * Total value of stock at the given date in the original currency of the stock.
     *
     * @param date a date in the format yyyy-MM-dd
     * @return total value of the purchase at the current date.
     */
    private double calculateTotalStockValueAtDate(String date) {
        String currentDate = stockHistory.getValidStockMarketDay(date);
        return stockHistory.fetchStockPriceAtGivenDate(currentDate) * numberOfSharesInPurchase;
    }

    @Override
    public String toString() {
        return getNumberOfShares() + " shares of " + getStockSymbol() + " purchased on " + dateOfPurchase + ":" + "\n" +
                "Total market value at purchase: " + Math.round(getValueAtPurchase()) + "\n" +
                "Total current market value: " + Math.round(getCurrentValue()) + "\n";
    }
}
