package com.stock;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tools.HttpHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

class StockHistory {

    private String apiKey = "QV00CLNEHIM7C8IV";

    private final String stockSymbol;
    private final JsonObject stockInfo;

    /**
     * Only used for unit tests
     *
     * @param apiKey this will always be "demo" to not consume our calls quota.
     */
    void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    /**
     * Default constructor.
     *
     * @param stockSymbol Symbol of the stock to fetch.
     */
    StockHistory(String stockSymbol) {
        this.stockSymbol = stockSymbol;
        this.stockInfo = fetchStockInfo();
    }

    /**
     * Downloads the stock history using the api Alpha Vantage (https://www.alphavantage.co/documentation/)
     * apiKey = "QV00CLNEHIM7C8IV" or "8AA4KLHMT7APTP13"
     */
    void downloadStockHistory() {
        String response = HttpHelper.get("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol="
                + stockSymbol + "&outputsize=full&apikey=" + apiKey);
        String filePath = Objects.requireNonNull(this.getClass().getClassLoader().getResource("stockHistory/")).getPath()
                + stockSymbol + ".json";
        try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
            byte[] strToBytes = response.getBytes();
            outputStream.write(strToBytes);
        } catch (IOException e) {
            throw new IllegalStateException("There was an error writing the file " + filePath);
        }
        assert (new File(filePath)).exists();
    }

    /**
     * Get the stock related info in a time series including the last 20 years.
     */
    File getStockHistory() {

        String filePath = Objects.requireNonNull(this.getClass().getClassLoader().getResource("stockHistory/")).getPath()
                + stockSymbol + ".json";
        String dir = this.getClass().getClassLoader().getResource("stockHistory/").getFile();

        File file = new File(filePath);
        LocalDate today = LocalDate.now();
        LocalDate lastModification = Instant.ofEpochMilli(file.lastModified()).atZone(ZoneId.systemDefault()).toLocalDate();

        if (today.isAfter(lastModification)) {
            downloadStockHistory();
            file = new File(filePath);
        }

        assert (new File(filePath)).exists();
        return file;
    }

    /**
     * Fetches stock info from the cache file.
     */
    JsonObject fetchStockInfo() {
        String stockInfo;
        String filePath = "";
        try {
            filePath = getStockHistory().getCanonicalPath();
            stockInfo = new String(Files.readAllBytes(Paths.get(filePath)));
        } catch (IOException e) {
            throw new IllegalStateException("Unable to parse file " + filePath);
        }

        JsonParser parser = new JsonParser();
        JsonObject dailySeries = parser.parse(stockInfo).getAsJsonObject().getAsJsonObject("Time Series (Daily)");
        assert dailySeries != null;
        return dailySeries;
    }

    /**
     * Fetches the stock price at a specified date.
     *
     * @return the stock price in the default stock currency at the given date.
     */
    double fetchStockPriceAtGivenDate(String date) {
        JsonObject object = stockInfo.getAsJsonObject(getValidStockMarketDay(date));
        assert object != null;
        JsonElement jsonElement = object.get("4. close");
        assert jsonElement != null;
        return jsonElement.getAsDouble();
    }

    /**
     * The stock market is closed on the weekends and holidays.
     * We need to retrieve a valid date. we will go back one day from the given date until we find a valid stock market day
     *
     * @param date The date to retrieve the stock.
     * @return returns a valid stock market date.
     */
    String getValidStockMarketDay(String date) {
        JsonObject stockInfoOfDay = stockInfo.getAsJsonObject(date);
        LocalDate localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        while (stockInfoOfDay == null) {
            localDate = localDate.minusDays(1);
            stockInfoOfDay = stockInfo.getAsJsonObject(localDate.toString());
        }
        return localDate.toString();
    }

}
