package com.tools;

import okhttp3.*;

import java.io.IOException;

public class HttpHelper {

    private static OkHttpClient client = new OkHttpClient();

    public static String get(String url) {
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException ioe) {
            throw new RuntimeException(ioe.getMessage());
        }
    }
}
