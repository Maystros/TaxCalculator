package com.tools;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PropertiesHandler {
    private static Properties properties = new Properties();

    /**
     * Loads all properties files in the project.
     */
    private static void loadProperty() {

        try (Stream<Path> walk = Files.walk(Paths.get("./src/main/resources/"))) {

            List<String> result = walk.map(x -> x.toString())
                    .filter(f -> f.endsWith(".properties")).collect(Collectors.toList());

            for (String file : result) {
                try (InputStream input = new FileInputStream(file)) {
                    properties.load(input);
                }
            }
        }
        catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }


    /**
     * Fetches a property from the property file
     * @param propertyName Name of the property to fetch
     * @return the value of the property in the properties file
     */
    public static String getProperty(String propertyName) {
        if (properties.isEmpty()) {
            loadProperty();
        }
        return properties.getProperty(propertyName);
    }

}
