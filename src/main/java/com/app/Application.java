package com.app;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.models.RevenueTax;
import com.models.StockSale;
import com.models.StockTax;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static spark.Spark.*;

/**
 * @author sehouate
 */
public class Application {

    public static void main(String args[]) {

        Gson gson = new Gson();
        port(1234);
        options("/*", (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((req, res) -> {
            res.header("Access-Control-Allow-Origin", "*");
            res.type("application/json");
        });

        path("/tax", () -> {

            get("/income/:yourIncome", (req, res) ->
                gson.toJson(new RevenueTax(Double.parseDouble(req.params(":yourIncome"))), RevenueTax.class));

            post("/stock", (req, res) -> {
                if (req.body().contains("[")) {
                    Collection<StockSale> stockSales = gson.fromJson(req.body(), new TypeToken<List<StockSale>>() {
                    }.getType());
                    Collection<StockTax> stockTaxCollection = new ArrayList<>();
                    for (StockSale stockSale : stockSales) {
                        stockTaxCollection.add(new StockTax(stockSale));
                    }
                    return gson.toJsonTree(stockTaxCollection);
                } else {
                    StockSale saleModel = gson.fromJson(req.body(), StockSale.class);
                    return gson.toJson(new StockTax(saleModel), StockTax.class);
                }
            });
        });
    }

}
