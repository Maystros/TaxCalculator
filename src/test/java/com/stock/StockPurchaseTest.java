package com.stock;

import org.testng.annotations.Test;

import java.util.ArrayList;

public class StockPurchaseTest {

    @Test
    @SuppressWarnings("unchecked")
    public void testGetValueAtPurchase() {
        ArrayList<StockPurchase> purchases = new ArrayList();

        //RSU
        purchases.add(new StockPurchase("ADBE", "2020-01-24", 49));

        purchases.add(new StockPurchase("ADBE", "2021-01-24", 33));
        purchases.add(new StockPurchase("ADBE", "2021-04-24", 8));
        purchases.add(new StockPurchase("ADBE", "2021-07-24", 8));
        purchases.add(new StockPurchase("ADBE", "2021-10-24", 8));

        //ESPP
        purchases.add(new StockPurchase("ADBE", "2019-06-28", 64));
        purchases.add(new StockPurchase("ADBE", "2019-12-31", 10));
        purchases.add(new StockPurchase("ADBE", "2020-06-30", 36));
        purchases.add(new StockPurchase("ADBE", "2020-12-31", 35));
        purchases.add(new StockPurchase("ADBE", "2021-06-30", 33.3366));
        purchases.add(new StockPurchase("ADBE", "2021-12-31", 24.4174));

        double totalValue = 0;
        double totalValueAtPurchase = 0;
        double totalGains = 0;
        double totalTaxes = 0;
        double totalGridTaxes = 0;

        StockSale stockSale;
        for (StockPurchase purchase : purchases) {
//            System.out.println(purchase.toString());
            stockSale = new StockSale(purchase);
            totalValueAtPurchase += purchase.getValueAtPurchase();
            totalValue += purchase.getCurrentValue();
            totalGains += stockSale.getGains();
            totalTaxes += stockSale.getFlatTax();
            totalGridTaxes += stockSale.getGridTax();
        }

//        System.out.println("Total value at purchase is " + Math.round(totalValueAtPurchase));
//        System.out.println("Total current gains is " + Math.round(totalGains));
        System.out.println("Total current value is " + Math.round(totalValue));
        System.out.println("Total flat taxes is " + Math.round(totalTaxes));
        System.out.println("Total value after flat tax is " + Math.round(totalValue - totalTaxes));
//        System.out.println("Total grid taxes is " + Math.round(totalGridTaxes));
//        System.out.println("Total value after grid tax is " + Math.round(totalValue - totalGridTaxes));
//        System.out.println("Total gains after taxes is " + Math.round(totalGains - totalTaxes));
//        System.out.println("In case of problems you'll have to pay " + Math.round(totalTaxes - totalGridTaxes));
    }

    @Test
    public void testGetCurrentValue() {
    }

    @Test
    public void testGetGains() {
    }

    @Test
    public void testGetTaxes() {
    }

    @Test
    public void testTestToString() {
    }
}