package com.stock;

import org.testng.annotations.Test;

import java.io.File;
import java.util.Objects;

/**
 * @author sehouate
 */
public class StockHistoryTest {

    private final String filePath = Objects.requireNonNull(this.getClass().getClassLoader().getResource("stockHistory/")).getPath()
            + "MSFT.json";

    @Test
    public void testDownloadStockHistory() {
        StockHistory sh = new StockHistory("MSFT");
        sh.setApiKey("demo");
        //The assertion is done in the method itself
        sh.downloadStockHistory();
        File file = new File(filePath);
        assert file.delete();
    }

    @Test
    public void testGetStockHistory() {
        StockHistory sh = new StockHistory("MSFT");
        //The assertion is done in the method itself
        sh.getStockHistory();
        File file = new File(filePath);
        assert file.delete();
    }

    @Test
    public void testFetchStockInfo() {
        StockHistory sh = new StockHistory("MSFT");
        //The assertion is done in the method itself
        sh.fetchStockInfo();
        File file = new File(filePath);
        assert file.delete();
    }

    @Test
    public void testFetchStockPriceAtGivenDate() {
        StockHistory sh = new StockHistory("MSFT");
        assert (sh.fetchStockPriceAtGivenDate("2019-07-22") == 138.4300);
    }

    @Test
    public void testGetValidStockMarketDay() {
        StockHistory sh = new StockHistory("MSFT");
        assert sh.getValidStockMarketDay("2019-11-24").contentEquals("2019-11-22");
    }
}