package com.stock;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ExchangeRateTest {

    @Test
    public void testConvertToTargetCurrency() {
        ExchangeRate exchangeRateDefault = new ExchangeRate();
        Assert.assertEquals(exchangeRateDefault.convertToTargetCurrency(1), 0.861585);

        ExchangeRate exchangeRateWithDate = new ExchangeRate("2019-11-25");
        Assert.assertEquals(exchangeRateWithDate.convertToTargetCurrency(1), 0.90842);

        ExchangeRate exchangeRateWithDateAndCurrency = new ExchangeRate("USD", "EUR", "2019-11-25");
        Assert.assertEquals(exchangeRateWithDateAndCurrency.convertToTargetCurrency(1), 0.90842);

        ExchangeRate exchangeRateWithDateAndSameCurrency = new ExchangeRate("USD", "USD", "2019-11-25");
        Assert.assertEquals(exchangeRateWithDateAndSameCurrency.convertToTargetCurrency(1), 1d);

    }
}