package com.stock;

import com.tools.DateHelper;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class LaTourelleStock {

    @Test
    public void test() {
        System.out.println(new ExchangeRate("2022-01-10").convertToTargetCurrency(8416));
        StockPurchase sp= new StockPurchase("ADBE",	"2016-01-15", 20);
        System.out.println("Stock purchase value at purchase " + sp.getValueAtPurchase());
        System.out.println("Stock purchase value at sale " + sp.getValueAtDate("2022-01-10"));

        StockSale ss = new StockSale(sp, "2022-01-10");
        System.out.println(ss.getGains());
        System.out.println(ss.getFlatTax());
        System.out.println(ss.getGridTax());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testGetValueAtPurchase() {
        ArrayList<StockPurchase> purchases = new ArrayList();

        purchases.add(new StockPurchase("ADBE",	"2019-01-24", 22));
        purchases.add(new StockPurchase("ADBE",	"2018-01-15", 20));
        purchases.add(new StockPurchase("ADBE",	"2016-01-15", 20));
        purchases.add(new StockPurchase("ADBE",	"2019-01-24", 78));
        purchases.add(new StockPurchase("ADBE",	"2016-12-31", 29));
        purchases.add(new StockPurchase("ADBE",	"2016-06-30", 48));
        purchases.add(new StockPurchase("ADBE",	"2018-01-24", 31));
        purchases.add(new StockPurchase("ADBE",	"2018-01-24", 111));
        purchases.add(new StockPurchase("ADBE",	"2017-01-15", 20));
        purchases.add(new StockPurchase("ADBE",	"2018-12-31", 58));
        purchases.add(new StockPurchase("ADBE",	"2019-01-15", 14));
        purchases.add(new StockPurchase("ADBE",	"2017-12-29", 45));
        purchases.add(new StockPurchase("ADBE",	"2017-06-30", 39));
        purchases.add(new StockPurchase("ADBE",	"2017-01-24", 31));
        purchases.add(new StockPurchase("ADBE",	"2018-06-29", 62));

        double totalValue = 0;
        double totalValueAtPurchase = 0;
        double totalGains = 0;
        double totalTaxes = 0;
        double totalGridTaxes = 0;

        StockSale stockSale;
        //System.out.println("acquisition date|sell date|# stock|value at purchase|value at sale|gain");
        for (StockPurchase purchase : purchases) {
            stockSale = new StockSale(purchase, "2022-01-12");
            System.out.println();
            totalValueAtPurchase += purchase.getValueAtPurchase();
            totalValue += stockSale.getSaleValue();
            totalGains += stockSale.getGains();
            totalTaxes += stockSale.getFlatTax();
            totalGridTaxes += stockSale.getGridTax();
            //System.out.println(purchase.getDateOfPurchase() + "|2022-01-10|" + Math.round(purchase.getNumberOfShares()) + "|" + Math.round(purchase.getValueAtPurchase()) + "|" + Math.round(purchase.getValueAtDate("2022-01-10")) + "|" +  Math.round(purchase.getValueAtDate("2022-01-10")-purchase.getValueAtPurchase()));
        }

//        System.out.println("Total value at purchase is " + Math.round(totalValueAtPurchase));
//        System.out.println("Total value when sold " + Math.round(totalValue));
        System.out.println("Total gains when sold " + Math.round(totalGains));
        System.out.println("Total flat taxes is " + Math.round(totalTaxes));
//        System.out.println("Total value after flat taxes " + Math.round(totalValue - totalTaxes));
        System.out.println("Total grid taxes is " + Math.round(totalGridTaxes));
//        System.out.println("Total value after grid tax is " + Math.round(totalValue - totalGridTaxes));
//        System.out.println("Total gains after taxes is " + Math.round(totalGains - totalTaxes));
//        System.out.println("In case of problems you'll have to pay " + Math.round(totalTaxes - totalGridTaxes));
//        System.out.println(153905 + 124630);

    }
    @Test
    public void testGetGains() {
        System.out.println(new ExchangeRate("USD", "EUR", "2016-01-15").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2017-01-15").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2018-01-15").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2019-01-15").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2017-01-24").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2018-01-24").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2019-01-24").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2018-01-24").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2019-01-24").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2016-06-30").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2016-12-31").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2017-06-30").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2017-12-29").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2018-06-29").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("USD", "EUR", "2018-12-31").convertToTargetCurrency(1));

        System.out.println(new ExchangeRate("USD", "EUR", "2022-01-10").convertToTargetCurrency(1));

    }

    @Test
    public void testGetTaxes() {
        System.out.println(new StockHistory("ADBE").fetchStockPriceAtGivenDate("2022-01-10"));
        ArrayList<StockPurchase> purchasesRsu = new ArrayList();
        //RSU
        purchasesRsu.add(new StockPurchase("ADBE", "2016-01-15", 20));
        purchasesRsu.add(new StockPurchase("ADBE", "2017-01-15", 20));
        purchasesRsu.add(new StockPurchase("ADBE", "2018-01-15", 20));
        purchasesRsu.add(new StockPurchase("ADBE", "2019-01-15", 14));
        purchasesRsu.add(new StockPurchase("ADBE", "2017-01-24", 31));
        purchasesRsu.add(new StockPurchase("ADBE", "2018-01-24", 31));
        purchasesRsu.add(new StockPurchase("ADBE", "2019-01-24", 22));
        purchasesRsu.add(new StockPurchase("ADBE", "2018-01-24", 111));
        purchasesRsu.add(new StockPurchase("ADBE", "2019-01-24", 78));

        //ESPP
        ArrayList<StockPurchase> purchasesESPP = new ArrayList();
        purchasesESPP.add(new StockPurchase("ADBE", "2016-06-30", 48));
        purchasesESPP.add(new StockPurchase("ADBE", "2016-12-31", 29));
        purchasesESPP.add(new StockPurchase("ADBE", "2017-06-30", 39));
        purchasesESPP.add(new StockPurchase("ADBE", "2017-12-29", 45));
        purchasesESPP.add(new StockPurchase("ADBE", "2018-06-29", 62));
        purchasesESPP.add(new StockPurchase("ADBE", "2018-12-31", 58));

        StockSale ss;
        System.out.println("# titres|prix d'acquisition|prix de cession|gain");
        for (StockPurchase sp : purchasesRsu) {
            ss = new StockSale(sp, "2022-01-10");
            System.out.print(Math.round(sp.getNumberOfShares()) + "|");
            System.out.print(Math.round(sp.getValueAtPurchase()) + "|");
            System.out.print(Math.round(ss.getSaleValue()) + "|");
            System.out.print(Math.round(ss.getSaleValue()/sp.getNumberOfShares()) + "|");
            System.out.println(Math.round(ss.getGains()));
        }

        for (StockPurchase sp : purchasesESPP) {
            ss = new StockSale(sp, "2022-01-10");
            System.out.print(Math.round(sp.getNumberOfShares()) + "|");
            System.out.print(Math.round(sp.getValueAtPurchase()) + "|");
            System.out.print(Math.round(ss.getSaleValue()) + "|");
            System.out.println(Math.round(ss.getGains()));
        }

    }

    @Test
    public void calcul(){
        System.out.println(new ExchangeRate("2022-01-10").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("2022-01-12").convertToTargetCurrency(1));
        System.out.println(new ExchangeRate("2022-01-10").convertToTargetCurrency(16*2));
        System.out.println(new ExchangeRate("2022-01-10").convertToTargetCurrency(320248));
    }
}