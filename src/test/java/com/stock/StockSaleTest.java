package com.stock;

import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class StockSaleTest {

    @Test
    @SuppressWarnings("unchecked")
    public void testGetValueAtPurchase() {
        ArrayList<StockPurchase> purchases = new ArrayList();

        purchases.add(new StockPurchase("ADBE",	"2019-01-24", 22));
        purchases.add(new StockPurchase("ADBE",	"2018-01-15", 20));
        purchases.add(new StockPurchase("ADBE",	"2016-01-15", 20));
        purchases.add(new StockPurchase("ADBE",	"2019-01-24", 78));
        purchases.add(new StockPurchase("ADBE",	"2016-12-31", 29));
        purchases.add(new StockPurchase("ADBE",	"2016-06-30", 48));
        purchases.add(new StockPurchase("ADBE",	"2018-01-24", 31));
        purchases.add(new StockPurchase("ADBE",	"2018-01-24", 111));
        purchases.add(new StockPurchase("ADBE",	"2017-01-15", 20));
        purchases.add(new StockPurchase("ADBE",	"2018-12-31", 58));
        purchases.add(new StockPurchase("ADBE",	"2019-01-15", 14));
        purchases.add(new StockPurchase("ADBE",	"2017-12-29", 45));
        purchases.add(new StockPurchase("ADBE",	"2017-06-30", 39));
        purchases.add(new StockPurchase("ADBE",	"2017-01-24", 31));
        purchases.add(new StockPurchase("ADBE",	"2018-06-29", 62));

        double totalCurrentValue = 0;
        double totalValueAtPurchase = 0;
        double totalValueAtSale = 0;
        double totalGains = 0;
        double totalTaxes = 0;
        double totalGridTaxes = 0;

        StockSale stockSale;
        for (StockPurchase purchase : purchases) {
//            System.out.println(purchase.toString());
            stockSale = new StockSale(purchase, "2022-01-10");
            totalValueAtPurchase += purchase.getValueAtPurchase();
            totalCurrentValue += purchase.getCurrentValue();
            totalValueAtSale += purchase.getValueAtDate("2022-01-10");
            totalGains += stockSale.getGains();
            totalTaxes += stockSale.getFlatTax();
            totalGridTaxes += stockSale.getGridTax();
        }

        System.out.println("Total value at purchase is " + Math.round(totalValueAtPurchase));
        System.out.println("Total value when sold:  " + Math.round(totalValueAtSale));
        System.out.println("Total flat taxes is " + Math.round(totalTaxes));
        System.out.println("Total value after flat tax is " + Math.round(totalValueAtSale - totalTaxes));
//        System.out.println("Total grid taxes is " + Math.round(totalGridTaxes));
//        System.out.println("Total value after grid tax is " + Math.round(totalCurrentValue - totalGridTaxes));
        System.out.println("Total gains after taxes is " + Math.round(totalGains - totalTaxes));
//        System.out.println("In case of problems you'll have to pay " + Math.round(totalTaxes - totalGridTaxes));
    }
    @Test
    public void testGetGains() {
    }

    @Test
    public void testGetTaxes() {
    }
}