package com.revenue;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TaxCalculatorTest {

    @Test
    public void calculateTax() {
        System.out.println("You are paying: " + (new TaxCalculator(82365.41).calculateTaxOnRevenue()));
        System.out.println("You are paying: " + (new TaxCalculator(80833).calculateTaxOnRevenue()));
    }

    @Test
    public void testCalculateTaxToPay() {
        Assert.assertEquals(new TaxCalculator(81000).calculateTaxOnRevenue(), 15378.0);
        System.out.println("Total taxes stock " + (57476));
        System.out.println("Total taxes salary " + (new TaxCalculator(79464.29).calculateTaxOnRevenue()));
        System.out.println("Total taxes stock + salary " + (57476 + new TaxCalculator(79464.29).calculateTaxOnRevenue()));
        System.out.println("Total taxes stock + salary left " + (57476 + new TaxCalculator(79464.29).calculateTaxOnRevenue() - 38181));
    }
}