This project helps calculate the taxes.
This includes taxes on income and taxes on stock purchase.

##Features:
- Calculate taxes on stock (https://www.impots.gouv.fr/portail/particulier/questions/mon-entreprise-ma-attribue-des-actions-gratuites-comment-sera-impose-le-gain).
- Support exchange rates at the acquisition and selling of the stock to make the gain and tax calculations 100% accurate (and legal). 
- Exchange rates are always up to date with the CEB.
- Allow calculation of gains and taxes at any given date (yeah, I saw you coming, not in the future you idiot!). 

##Limitations:
- The calculations for taxes on the revenue are for a single person with no children with the default 10% tax discount (this is a property that is easily updated).
- The calculations for the taxes on the gains relative to a stock sale are calculated using the flat tax (30%).
- This is not idiot proof (dates should be formatted correctly, only real currencies are supported...).

##Next improvements:
- Expose tax calculation as a REST API.
 
